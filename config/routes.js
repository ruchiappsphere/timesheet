/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'homepage'
  },

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/
 //Client details
'POST /client/addClient': {controller:'ClientController',action:'addClient'},
'GET /client/findAllClient':{controller:'ClientController',action:'findAllClient'},
'GET /client/findClient/:id': {controller:'ClientController',action:'findClient'},
'PUT /client/editClient/:id':{controller:'ClientController',action:'editClient'},
'PUT /client/deleteClient/:id':{controller:'ClientController',action:'deleteClient'},
'GET /client/pastClient':{controller:'ClientController',action:'pastClient'},
'PUT /client/rejoinClient/:id':{controller:'ClientController',action:'rejoinClient'},
'GET /client/findName':{controller:'ClientController',action:'findName'},

//consultant details
'POST /consultant/addConsultant': {controller:'ConsultantsController',action:'addConsultant'},
'GET /consultant/findAllConsultant':{controller:'ConsultantsController',action:'findAllConsultant'},
'GET /consultant/searchConsultant/:id': {controller:'ConsultantsController',action:'searchConsultant'},
'GET /consultant/getConsultantName':{controller:'ConsultantsController',action:'getConsultantName'},
'PUT /consultant/editConsultant/:id':{controller:'ConsultantsController',action:'editConsultant'},
'DELETE /consultant/deleteConsultant/:id':{controller:'ConsultantsController',action:'deleteConsultant'},
'GET /consultant/pastConsultant':{controller:'ConsultantsController',action:'pastConsultant'},
'PUT /consultant/rejoinConsultant/:id':{controller:'ConsultantsController',action:'rejoinConsultant'},

//Project details
'POST /project/addProject': {controller:'ProjectController',action:'addProject'},
'GET /project/findClientsProj/:id':{controller:'ProjectController',action:'findClientsProj'},
'GET /project/showProject/:id':{controller:'ProjectController',action:'showProject'},
'GET /project/findAllProject':{controller:'ProjectController',action:'findAllProject'},
'GET /project/findPastProject':{controller:'ProjectController',action:'findPastProject'},
'PUT /project/updateProject/:id':{controller:'ProjectController',action:'updateProject'},
'GET /project/getProjectName':{controller:'ProjectController',action:'getProjectName'},

//Task details
'POST /task/addTask':{controller:'TasksController',action:'addTask'},
'PUT /task/editTask/:id':{controller:'TasksController',action:'editTask'},
'DELETE /task/deleteTask/:id':{controller:'TasksController',action:'deleteTask'},
'PUT /task/TaskDone/:id':{controller:'TasksController',action:'TaskDone'},
'GET /task/consultantTasks/:id':{controller:'TasksController',action:'consultantTasks'},
'GET /task/ProjectTasks/:id':{controller:'TasksController',action:'ProjectTasks'},
'POST /task/getTasks':{controller:'TasksController',action:'getTasks'},
'POST /task/taskWorking':{controller:'TasksController',action:'taskWorking'},

//Leave details
'POST /leave/addLeave':{controller:'LeavesController',action:'addLeaves'},
'DELETE /leave/deleteLeave/:id':{controller:'LeavesController',action:'deleteLeave'},
'PUT /leave/updateLeave/:id':{controller:'LeavesController',action:'updateLeave'},

//Working details
'POST /working/addWorking':{controller:'WorkingController',action:'addWorking'},
'PUT /working/updateWorking':{controller:'WorkingController',action:'updateWorking'},
'POST /working/deleteWorking':{controller:'WorkingController',action:'deleteWorking'},
'GET /working/workingTask/:id':{controller:'WorkingController',action:'workingTask'},
'PUT /working/addProjectWork': {controller:'WorkingController',action:'addProjectWork'},
};
