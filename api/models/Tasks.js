/**
 * Tasks.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var moment=require('moment');
module.exports = {
  connection:'PostgresqlServer',
    tableName:'task_details',
    autoCreatedAt:'created_at',
    autoUpdatedAt:'updated_at',
    autoPK: false,
    attributes: {
      
      /*schema: true,*/
      id:
      {
        type: 'number',
        columnName: 'task_id',
         required: true,
         primaryKey: true,
         autoIncrement:true
        
          },
          task_allocated:{
            type:'string',
            required:false
          },
          hours_required:{
            type:'integer',
            required:false
          },
          task_start_date:{
              type:'date',
              required:true
          },
          task_end_date:{
            type:'date',
            required:true
          },
          is_completed:{
            type:'boolean',
            required:false,
            defaultsTo:false
          },
          completed_date:
          {
            type:'date',
            required:false
          },
          created_by:{
            type:'string',
            required:false
          }, 
          updated_by:{
            type:'string',
            required:false
          },
          project_details_project_id:
          {type:'integer',
          required:false
          }, 
          consultant_detail_consultant_id:
          {
            type:'integer',
            required:true
          },
          
          total_hours:function(){
            var date1 = moment(this.task_start_date);
            var date2 = moment(this.task_end_date);
            var bh = date2.diff(date1,'hours');
            return bh;
          },

          toJSON: function() {
            var obj = this.toObject();
            obj.total_hours = this.total_hours();
            return obj;
          },
          days:{
            collection:'Working',
            via:'task_details_task_id'
          }
          
 },
        beforeValidate: function(values, cb) {
          if (isNaN(values.project_details_project_id)||isNaN(values. consultant_detail_consultant_id))
                   {
                    console.log ('Project identification or Consultant Identification is required ');
                    var err=new Error("Project identification or Consultant Identification is required");
                  cb( err);    
                   }
          if ((moment(values.task_end_date).utc().isBefore(moment(values.task_start_date).utc()))||
                (moment(values.task_start_date).utc().isBefore(moment().utc())))
              {
                  console.log ('Start date is after End date or Start Date is not proper');
                  var err=new Error("Task start date and Task End date are not proper or start date is not proper");
                cb( err); 
                }
                cb();
                }
};

