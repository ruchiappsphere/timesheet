/**
 * Client.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  connection:'PostgresqlServer',
  tableName:'client_detail',
  autoCreatedAt:'created_at',
  autoUpdatedAt:'updated_at',
attributes:
{
  
      id:
      {
        type: 'number',
        columnName: 'client_id',
         required: true,
         autoIncrement:true,
       },
          client_name:{
            type:'string',
            required:false
          } ,
          company_name:{
              type:'string',
              required:false
          },
          client_email_id:{
              type:'string',
              required:false
          }, 
          client_phone_num:{
              type:'string',
              required:false
          }, 

          created_by:{
            type:'string',
            required:false
          }, 
          updated_by:{
              type:'string',
              required:false
          },
          is_active:{
              type:'boolean',
              required:false,
              defaultsTo: true
          },
          client_add_1:{
            type:'text',
            required:false
            },			
          client_add_2:{
              type:'text',
              required:false
            },			
          client_postal_code:{
                type:'string',
                required:false
            },			
          client_city:{
                type:'string',
                required:false
            },			
            client_state:{
                type:'string',
                required:false
            },			
            client_country:{
              type:'string',
                required:false
            },			
            client_gst_num:{
              type:'string',
                required:false
            },			
            client_url:{
              type:'string',
                required:false
            }
          },
             
            validationMessages: {
                client_id: {
                  required: 'Client ID is required'
              } 
  }

};

