/**
 * Leaves.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  connection:'PostgresqlServer',
  tableName:'leave_details',
  autoCreatedAt:'created_at',
  autoUpdatedAt:'updated_at',


  attributes: {
    id:{
      type: 'number',
      columnName:'leave_proj_consult_id',
      required: true,
      autoIncrement:true
    },			
    leave_date:{
      type:'date',
      required:true
    },			
    leave_type:{
      type:'string',
      required:false
    },			
    created_by:{
      type:'string',
      required:false
    },			
    updated_by:{
      type:'string',
      required:false
    },
    task_details_task_id:{
      type:'integer',
      required:true
    }    

  }
};

