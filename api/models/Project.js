/**
 * Project.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var moment=require('moment');
  module.exports = {

    connection:'PostgresqlServer',
    tableName:'project_details',
    autoCreatedAt:'created_at',
    autoUpdatedAt:'updated_at',

    attributes: {
      id:
      {
        type: 'number',
        primaryKey: true,
        columnName: 'project_id',
         required: true,
         autoIncrement:true
          },
          project_name:{
            type:'string',
            required:false
          },
          project_description:{
            type:'text',
            required:false
          },
          project_start_date:{
            type:'date',
            required:false
          },
          project_end_date:
          {type:'date',
          required:false
          },
          is_completed:{
            type:'boolean',
            required:false,
            defaultsTo:false
          },
          completed_date:
          {
            type:'date',
            required:false
          },
          created_by:{
            type:'string',
            required:false
          }, 
          updated_by:{
            type:'string',
            required:false
          }, 
          client_detail_client_id:
          {
            type:'integer',
            required:true
          },
          total_billable_hours:function(){
            var date1 = moment(this.project_start_date);
            var date2 = moment(this.project_end_date);
            var bh = date2.diff(date1,'hours');
            
            return bh;
          },

          toJSON: function() {
            var obj = this.toObject();
            obj.total_billable_hours = this.total_billable_hours();
            return obj;
          }


          
      },
    
    beforeValidate: function(values, cb) {
    if ((moment(values.project_end_date).utc().isBefore(moment(values.project_start_date).utc()))||
          (moment(values.project_start_date).utc().isBefore(moment().utc())))
        {
            console.log ('Start date is after End date or Start Date is not proper');
            var err=new Error("Project start date and Project End date are not proper or start date is not proper");
          cb( err); 
          }
          cb();
          }
};

