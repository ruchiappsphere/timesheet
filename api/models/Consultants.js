/**
 * Consultants.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  connection:'PostgresqlServer',
  tableName:'consultant_detail',
  autoCreatedAt:'created_at',
  autoUpdatedAt:'updated_at',
attributes:
{
  
      id:
      {
        type: 'number',
        columnName: 'consultant_id',
         required: true,
         autoIncrement:true
          },
          consultant_name:{
            type:'string',
            required:false
          } ,
          work_location:{
              type:'string',
              required:false
          },
          consultant_email_id:{
              type:'string',
              required:false
          }, 
          consultant_phone_num:{
              type:'string',
              required:false
          }, 
          created_by:{
            type:'string',
            required:false
          }, 
          updated_by:{
              type:'string',
              required:false
          },
        acc_no:{
            type:'string',
            required:false
        },
      ifsc_code:{
          type:'string',
          required:false
        },
        is_active:{
            type:'boolean',
            required:false,
            defaultsTo: true
      }  
    }

};



