var moment=require('moment');
var apiResponse = require('../apiResponse/response.js');
module.exports = {
    addWorking: function(req,res,next){
        console.log(req.body.leave_type)
                if (!req.body.leave_type)
                {
                    try{
                        Working.findOrCreate(req.body).exec(function (err,result){
                            if (err) {
                            sails.log.debug('Some error occured ' + err);
                            return res.json(500, { error: err });
                        
                            }
                        return res.json(apiResponse.success(result, "Work Added Successfully"));
                    });
                    
                    var w_date=req.body.work_date;
                    console.log();
                    for(i=0; i<=6;i++)
                    { 
                            var w_date= moment(w_date).add(1,'days').format('YYYY-MM-DD');
                            Working.findOrCreate({work_date:w_date,task_details_task_id:req.body.task_details_task_id}).exec(function (err,result){
                                if (err) {
                                sails.log.debug('Some error occured ' + err);
                                return res.json(500, { error: err });
                                }
                                
                                //res.json(apiResponse.success(result, "Work Added Successfully"));
                            });
                        
                    }
                    
                }catch(ex){
                    console.log("inside Wokring exception:::::::::" + ex);
                }
            }
        
            else{
                   Working.update({task_details_task_id:req.body.task_details_task_id , work_date:req.body.work_date},req.body).exec(function (err,result){
                        if (err) {
                        sails.log.debug('Some error occured ' + err);
                        return res.json(500, { error: err });
                    
                        }
                    return res.json(apiResponse.success(result, "Leave Added Successfully"));
                });
                }
            
                
            },
        
    updateWorking: function(req, res, next) {
        Working.update({task_details_task_id:req.body.task_details_task_id , work_date:req.body.work_date},req.body).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },

    deleteWorking:function(req,res,next){
        Working.destroy({task_details_task_id:req.body.task_details_task_id , work_date:req.body.work_date}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },
    workingTask: function(req,res,next){
        Working.find({task_details_task_id:req.param('id')}).sort({work_date: 'ASC'}).exec(function (err, result)  {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },

    addProjectWork: function(req,res,next){
        Working.findOrCreate(req.body)
        .exec(function (err,result,wasCreated){
            console.log(wasCreated);
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            if(wasCreated) {
                return res.json(apiResponse.success(result, "Work Added Successfully"));
              }
              else {
                return res.json(apiResponse.success(result, "Work found"));
              }
        });

    }

    

	
};

