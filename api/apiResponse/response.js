module.exports = {
    success : function (result, message) {
		if(!message){
			message="Success"
		}
        var successResult = {
             status: 1,
             message: message,
             data : result
        }
        return successResult;
    },

    failure : function (error) {
        var errorResult = {
             status: 0,
             message: error
        }
        return errorResult;
    }  
};