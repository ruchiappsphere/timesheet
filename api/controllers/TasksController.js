/**
 * TasksController
 *
 * @description :: Server-side logic for managing Tasks
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var apiResponse = require('../apiResponse/response.js');
module.exports = {
    
    addTask: function(req,res,next){
        if ((!req.body.project_details_project_id)||(!releaseEvents.body.consultant_detail_consultant_id))
        {
            return res.badRequest(new Error('Project or Consultant identification is required'));
        }
     
         try{
            Tasks.create(req.body).exec(function (err,result){
            if (err) {
               sails.log.debug('Some error occured ' + err);
              return res.json(500, { error: err });
          }
          return res.json(apiResponse.success(result, "Task Added Successfully"));
        });        

    }catch(ex)
    {return res.json(apiResponse.failure(ex.message)); }
    },
    
    editTask:function(req,res,next){
        Tasks.update({id:req.param('id')},req.body).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
      },
      
    deleteTask:function(req,res,next){
        Tasks.destroy({id : req.param('id')}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },

    TaskDone:function(req,res,next){
        Tasks.update({id:req.param('id')},{is_completed:true,completed_date:new Date()})
        .exec(function (err, result) {
        if (err) {
            sails.log.debug('Some error occured ' + err);
            return res.json(500, { error: err });
        }
        
        return res.json(apiResponse.success(result));
            });
    },

    
    consultantTasks: function(req,res,next){
          Tasks.find({consultant_detail_consultant_id:req.param('id')}).exec(function (err, result)  {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },

    ProjectTasks: function(req,res,next){
        Tasks.find({project_details_project_id:req.param('id')}).exec(function (err, result)  {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },
    
    getTasks:function(req,res,next){
        var proj_det_id=req.body.project_details_project_id;
        var consultant_det_id=req.body.consultant_detail_consultant_id;
        Tasks.find({project_details_project_id:proj_det_id})
        .where({consultant_detail_consultant_id:consultant_det_id})
           .exec(function(err, found){
            if(err){
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
           }
           return res.json(apiResponse.success(found));
        });
    },
    taskWorking: function(req,res,next){
               Tasks.find({id:req.body.task_details_task_id}).populate('days').exec(function (err, result)  {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    }
};

