              return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },
    workingTask: function(req,res,next){
        Working.find({task_details_task_id:req.param('id')}).sort({work_date: 'ASC'}).exec(function (err, result)  {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },

    addProjectWork: function(req,res,next){
        Working.findOrCreate(req.body)
        .exec(function (err,result,wasCreated){
            console.log(wasCreated);
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            if(wasCreated) {
                return res.json(apiResponse.success(result, "Work Added Successfully"));
              }
              else {
                return res.json(apiResponse.success(result, "Work found"));
              }
        });

    }

    

	
};