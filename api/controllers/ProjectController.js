/**
 * Project
 *
 * @description :: Server-side logic for managing Project
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var moment=require('moment');
 var apiResponse = require('../apiResponse/response.js');
module.exports = {
    addProject: function(req,res,next){
         Project.create(req.body).exec(function (err,result){
            if (err) {
               sails.log.debug('Some error occured ' + err);
              return res.json(500, { error: err });
          }          return res.json(apiResponse.success(result, "Project Added Successfully"));
      });
    
    },

    showProject: function(req, res, next) {
        Project.findOneById(req.param('id'), function Founded(err, value) {
             if (err) {
               sails.log.debug('Some error occured ' + err);
              return res.json(500, { error: err });
          }
          return res.json(apiResponse.success(value));
        });
    },
    
    findPastProject: function(req,res,next){
    
        Project.find({is_completed:true}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }

            return res.json(apiResponse.success(result));
        });
    },


    findAllProject: function(req,res,next){
            Project.find({is_completed:false}).sort({created_at:'desc'}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
           
            return res.json(apiResponse.success(result));
        });
    },

    
    updateProject: function(req, res, next) {
        Project.update({id:req.param('id')},req.body).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },

    findClientsProj: function(req, res, next) {
     console.log(req.param('id'));
        Project.find({client_detail_client_id:req.param('id')}).exec(function (err, result)  {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },

    projectDone:function(req,res,next){
        Project.update({id:req.param('id')},{is_completed:true,completed_date:new Date()})
        .exec(function (err, result) {
        if (err) {
            sails.log.debug('Some error occured ' + err);
            return res.json(500, { error: err });
        }
        
        return res.json(apiResponse.success(result));
    });
},
getProjectName:function(req,res,next){
    Project.find({is_completed:false},{select: ['id', 'project_name']}).exec(function (err, result) {
        if (err) {
            sails.log.debug('Some error occured ' + err);
            return res.json(500, { error: err });
        }
        return res.json(apiResponse.success(result));
    });
}

};