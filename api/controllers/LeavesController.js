/**
 * LeavesController
 *
 * @description :: Server-side logic for managing leaves
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var apiResponse = require('../apiResponse/response.js');
module.exports = {
    addLeaves: function(req,res,next){
        console.log('i am here');
    Leaves.create(req.body).exec(function (err,result){
        if (err) {
           sails.log.debug('Some error occured ' + err);
          return res.json(500, { error: err });
      }
      return res.json(apiResponse.success(result, "Leave Added Successfully"));
  });
},
updateLeave: function(req, res, next) {
    Leaves.update({id:req.param('id')},req.body).exec(function (err, result) {
        if (err) {
            sails.log.debug('Some error occured ' + err);
            return res.json(500, { error: err });
        }
        return res.json(apiResponse.success(result));
    });
},
deleteLeave:function(req,res,next){
    Leaves.destroy({id : req.param('id')}).exec(function (err, result) {
        if (err) {
            sails.log.debug('Some error occured ' + err);
            return res.json(500, { error: err });
        }
        return res.json(apiResponse.success(result));
    });
},



};

