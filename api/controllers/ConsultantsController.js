/**
 * ConsultantsController
 *
 * @description :: Server-side logic for managing consultants
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var apiResponse = require('../apiResponse/response.js');
module.exports = {
    addConsultant:function(req,res,next){
        Consultants.create(req.body).exec(function (err,result){
            if (err) {
               sails.log.debug('Some error occured ' + err);
              return res.json(500, { error: err });
          }
          return res.json(apiResponse.success(result, "Consultant Added Successfully"));
      });
    },
    findAllConsultant: function(req,res,next){
            Consultants.find({is_active:true}).sort({created_at:'desc'}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },
    searchConsultant : function(req,res,next){
     Consultants.find({id:req.param('id')}).exec(function (err, result) {
        if (err) {
            sails.log.debug('Some error occured ' + err);
            return res.json(500, { error: err });
        }
        return res.json(apiResponse.success(result));
    });
},
    editConsultant: function(req,res,next){
        Consultants.update({id:req.param('id')},req.body).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
        },
    deleteConsultant: function(req,res,next){
        Consultants.update({id:req.param('id')},{is_active:false}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
        },
        pastConsultant: function(req,res,next){
            Consultants.find({is_active:false}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },  
    rejoinConsultant: function(req,res,next){
        Consultants.update({id:req.param('id')},{is_active:true}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
        },
    getConsultantName:function(req,res,next){
        Consultants.find({is_active:true},{select: ['id', 'consultant_name']}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });

    }



    }
