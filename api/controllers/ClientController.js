/**
 * ClientController
 *
 * @description :: Server-side logic for managing clients
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var apiResponse = require('../apiResponse/response.js');
module.exports = {
    addClient: function(req,res,next){
        Client.create(req.body).exec(function (err,result){
            if (err) {
               sails.log.debug('Some error occured ' + err);
              return res.json(500, { error: err });
          }
          return res.json(apiResponse.success(result, "Client Added Successfully"));
      });
    },
    findAllClient: function(req,res,next){
            Client.find({is_active:true}).sort({created_at:'desc'}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
    },

    findClient: function(req,res,next){
        const params= req.validate(['id'])
        console.log(params);    
        if (!(params)) {return;} 
      Client.find({id:req.param('id')}).exec(function (err, result) {
        if (err) {
            sails.log.debug('Some error occured ' + err);
            return res.json(500, { error: err });
        }
        return res.json(apiResponse.success(result));
    });    
           },

    editClient: function(req,res,next){
        Client.update({id:req.param('id')},req.body).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
     },
     deleteClient: function(req,res,next){
        Client.update({id:req.param('id')},{is_active:false},req.body).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
     },
     pastClient: function(req,res,next){
        Client.find({is_active:false}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
     },
     rejoinClient: function(req,res,next){
        Client.update({id:req.param('id')},{is_active:true}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
     },
    findName: function(req,res,next) {
        
        Client.find({is_active:true},{select: ['id', 'client_name']}).exec(function (err, result) {
            if (err) {
                sails.log.debug('Some error occured ' + err);
                return res.json(500, { error: err });
            }
            return res.json(apiResponse.success(result));
        });
     },
    
};

